package org.sas.eightqueens;

import static java.lang.Math.abs;

public class Queen {
	private int row, col;

	public Queen(int row, int col) {
		super();
		this.row = row;
		this.col = col;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + col;
		result = prime * result + row;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Queen other = (Queen) obj;
		if (col != other.col)
			return false;
		if (row != other.row)
			return false;
		return true;
	}

	public boolean attack(Queen queen) {
		return row == queen.row || col == queen.col || inDiagonal(queen);
	}

	private boolean inDiagonal(Queen queen) {
		return abs(row - queen.row) == abs(col - queen.col);
	}
	
}
