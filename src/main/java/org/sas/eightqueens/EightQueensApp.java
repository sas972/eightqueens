package org.sas.eightqueens;

import java.util.ArrayList;
import java.util.List;

public class EightQueensApp {

	private List<Board> result = new ArrayList<>();
	
	public void solve(int col, Board board){
		if(Board.SIZE == col)
			result.add(board.copy());
		else
			for(int row = 0; row < Board.SIZE; ++row){
				Queen queen = new Queen(row, col);
				if(board.isValid(queen)){
					board.add(queen);
					solve(col+1, board);
					board.remove(queen);
				}
			}
	}
	
	public static void main(String... strings){
		EightQueensApp app = new EightQueensApp();
		app.solve(0, new Board());
		System.out.println("Number of solution: " + app.result.size());
		System.out.println(app.result);
	}
}
