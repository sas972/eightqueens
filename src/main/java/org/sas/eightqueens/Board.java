package org.sas.eightqueens;

import static java.lang.System.getProperty;

import java.util.ArrayList;
import java.util.List;

public class Board {

	private static final String EMPTY = ". ";
	private static final String Q = "Q ";
	private static final String LINE_SEPARATOR = "line.separator";
	public static final int SIZE = 8;
	private List<Queen> queens = new ArrayList<>();
	
	private Board(Board board) {
		queens = new ArrayList<>(board.queens);
	}

	public Board() {
	}

	public Board copy() {
		return new Board(this);
	}

	public boolean isValid(Queen queen) {
		for ( Queen q : queens){
			if(q.attack(queen))
				return false;
		}
		return true;
	}

	public boolean add(Queen e) {
		return queens.add(e);
	}

	public boolean remove(Object o) {
		return queens.remove(o);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(getProperty(LINE_SEPARATOR));
		for(int row = 0; row < SIZE; ++row){
			for (int col = 0; col < SIZE; ++col){
				if(isQueen(row,col)){
					sb.append(Q);
				}
				else {
					sb.append(EMPTY);
				}
			}
			sb.append(getProperty(LINE_SEPARATOR));
		}
		return sb.toString();
	}

	private boolean isQueen(int row, int col) {
		Queen probe = new Queen(row, col);
		return queens.contains(probe);
	}

	
	
}
